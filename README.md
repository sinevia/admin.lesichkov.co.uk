# admin.lesichkov.com (v2021)

This is now deprecated. The new project is at: https://gitlab.com/sinevia/admin.sinevia.com

---

![Build Status](https://gitlab.com/pages/admin.lesichkov.co.uk/badges/master/build.svg)

---

Live URL: 
[https://admin-lesichkov-co-uk-6cz4k5d46q-ez.a.run.app/](https://admin-lesichkov-co-uk-6cz4k5d46q-ez.a.run.app/)

<a href="https://admin.lesichkov.co.uk/" target="_blank">
    https://admin.lesichkov.co.uk
</a>

GitLab URL
<a href="https://sinevia.gitlab.io/admin.lesichkov.co.uk/" target="_blank">
    https://sinevia.gitlab.io/admin.lesichkov.co.uk/
</a>

API Live URL

<a href="https://api-lesichkov-co-uk-uodwjwllpq-ez.a.run.app" target="_blank">
    https://api-lesichkov-co-uk-uodwjwllpq-ez.a.run.app
</a>

---

## Deployment

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

- The localhost development URLs are replaced with the LIVE URLs

## Development

```
npm run-script serve
```
