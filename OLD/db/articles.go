package db

import (
	"log"
	"strconv"

	"github.com/Masterminds/squirrel"
	"lesichkov.co.uk/app/helpers"
)

// TableArticle name of the cache table
const TableArticle = "snv_articles_article"

// TableArticleTranslation name of the article translation table
const TableArticleTranslation = "snv_articles_articletranslation"

// ArticleFindByID finds a user by email
func ArticleFindByID(id string) map[string]string {
	sqlStr, args, _ := squirrel.Select("*").From(TableArticle).Where(squirrel.Eq{"Id": id}).ToSql()

	entity := QuerySingle(sqlStr, args...)

	return entity
}

// ArticleList list articles
func ArticleList(offset uint64, limit uint64, search string, orderBy string, sort string) []map[string]string {
	qTranslations := squirrel.Select("ArticleId").From(TableArticleTranslation)

	if search != "" {
		qTranslations = qTranslations.Where(squirrel.Like{"Title": "%" + search + "%"})
	}

	sqlStr01, args, _ := qTranslations.ToSql()

	translationEntities := Query(sqlStr01, args...)
	articleIds := helpers.MapToColumn(translationEntities, "ArticleId")
	// articleIds := make([]string, 0)
	// for _, element := range translationEntities {
	// 	articleIds = append(articleIds, element["ArticleId"])
	// }

	// log.Println(articleIds)

	if search != "" && len(articleIds) < 1 {
		return make([]map[string]string, 0)
	}

	q := squirrel.Select("*").From(TableArticle).OrderBy(orderBy + " " + sort).Offset(offset).Limit(limit)

	if len(articleIds) > 0 {
		log.Println("with search")
		q = q.Where(squirrel.Eq{"Id": articleIds})
	}

	sqlStr, args, _ := q.ToSql()

	log.Println(sqlStr)

	entities := Query(sqlStr, args...)

	return entities
}

// ArticleCount counts articles
func ArticleCount() uint64 {
	sqlStr, args, _ := squirrel.Select("COUNT(*) AS count").From(TableArticle).Limit(1).ToSql()

	entities := Query(sqlStr, args...)

	count, _ := strconv.ParseUint(entities[0]["count"], 10, 64)

	return count
}

// ArticleTranslationFindByID finds a user by email
func ArticleTranslationFindByID(id string) map[string]string {
	sqlStr, args, _ := squirrel.Select("*").From(TableArticle).Where(squirrel.Eq{"Id": id}).ToSql()

	entity := QuerySingle(sqlStr, args...)

	return entity
}

// ArticleTranslationFindByArticleIDAndLanguage finds a user by email
func ArticleTranslationFindByArticleIDAndLanguage(articleID string, language string) map[string]string {
	sqlStr, args, _ := squirrel.Select("*").From(TableArticleTranslation).Where(squirrel.Eq{"ArticleId": articleID}).Where(squirrel.Eq{"Language": language}).ToSql()

	entity := QuerySingle(sqlStr, args...)

	return entity
}

// ArticleUpdate updates an article by ID
func ArticleUpdate(articleID string, data map[string]string) bool {
	mapInterface := make(map[string]interface{})
	for key, value := range data {
		mapInterface[key] = value
	}
	sqlStr, args, _ := squirrel.Update(TableArticle).Where(squirrel.Eq{"Id": articleID}).SetMap(mapInterface).ToSql()

	result := Exec(sqlStr, args...)

	return result
}

// ArticleTranslationUpdate updates an article translation by ID and language
func ArticleTranslationUpdate(articleID string, language string, data map[string]string) bool {
	mapInterface := make(map[string]interface{})
	for key, value := range data {
		mapInterface[key] = value
	}

	sqlStr, args, _ := squirrel.Update(TableArticleTranslation).Where(squirrel.Eq{"ArticleId": articleID}).Where(squirrel.Eq{"Language": language}).SetMap(mapInterface).ToSql()

	result := Exec(sqlStr, args...)

	return result
}
