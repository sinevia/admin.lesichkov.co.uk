// package db

// import (
// 	"github.com/Masterminds/squirrel"
// )

// // UserFindByEmail finds a user by email
// func UserFindByEmail(email string) map[string]string {
// 	sqlStr, args, _ := squirrel.Select("*").From("snv_users_user").Where(squirrel.Eq{"email": email}).ToSql()

// 	users := Query(sqlStr, args...)

// 	if len(users) > 0 {
// 		return users[0]
// 	}

// 	return nil
// }

package db

import (
	"fmt"
	"log"
	"time"

	"github.com/Masterminds/squirrel"
	"github.com/gouniverse/uid"
)

// TableCache name of the cache table
const TableCache = "snv_caches_cache"

// CacheFindByKey finds a cache row by key, if not expired
func CacheFindByKey(key string) map[string]string {
	sql, args, _ := squirrel.Select("*").From(TableCache).Where(squirrel.Eq{"cache_key": key}).Where(squirrel.Gt{"expires_at": time.Now().Format("2006-01-02 15:04:05")}).ToSql()
	return QuerySingle(sql, args...)
}

// CacheSet inserts a new key value pair with expiration time
func CacheSet(key string, value string, seconds int64) bool {
	expiresAt := time.Now().Add(time.Second * time.Duration(seconds))
	sql, args, _ := squirrel.Insert(TableCache).Columns("id", "cache_key", "cache_value", "expires_at", "created_at", "updated_at").Values(uid.NanoUid(), key, value, expiresAt.Format("2006-01-02 15:04:05"), time.Now().Format("2006-01-02 15:04:05"), time.Now().Format("2006-01-02 15:04:05")).ToSql()
	return Exec(sql, args...)
}

// CacheGet selects a cache by key
func CacheGet(key string, defaultValue string) string {
	cache := CacheFindByKey(key)

	if cache == nil {
		return defaultValue
	}

	log.Println(cache)
	return cache["cache_key"]
}

// CacheExpireJobGoroutine - soft deletes expired cache
func CacheExpireJobGoroutine() {
	i := 0
	for {
		i++
		fmt.Println("Cleaning expired cache...")
		sql, args, _ := squirrel.Delete(TableCache).Where(squirrel.LtOrEq{"expires_at": time.Now().Format("2006-01-02 15:04:05")}).ToSql()
		// log.Println(sql)
		// log.Println(args)
		Exec(sql, args...)
		time.Sleep(60 * time.Second) // Every minute
	}
}
