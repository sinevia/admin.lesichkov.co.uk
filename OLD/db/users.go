package db

import (
	"github.com/Masterminds/squirrel"
)

// UserFindByEmail finds a user by email
func UserFindByEmail(email string) map[string]string {
	sqlStr, args, _ := squirrel.Select("*").From("snv_users_user").Where(squirrel.Eq{"email": email}).ToSql()

	user := QuerySingle(sqlStr, args...)

	return user
}
