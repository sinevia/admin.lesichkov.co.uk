package routes

import (
	"log"
	"math"
	"net/http"
	"strconv"
	"strings"

	"github.com/gofiber/adaptor/v2"

	"github.com/gofiber/fiber/v2"
	"github.com/gouniverse/api"
	"github.com/gouniverse/utils"
	"lesichkov.co.uk/app/db"
	"lesichkov.co.uk/app/helpers"
)

// SetupArticleRoutes setups the article URLs
func SetupArticleRoutes(endpoint string, app *fiber.App, middleware fiber.Handler) {
	//api := app.Group("/api", logger.New(), middleware.AuthReq())
	api := app.Group(endpoint, middleware)

	api.Post("/article-create", adaptor.HTTPHandlerFunc(articleCreate))
	api.Post("/article-find", adaptor.HTTPHandlerFunc(articleFind))
	api.Post("/article-list", articleList)
	api.Post("/article-update", adaptor.HTTPHandlerFunc(articleUpdate))
}

// articleCreate creates a new article
func articleCreate(w http.ResponseWriter, r *http.Request) {
	//status := strings.Trim(helpers.Req(r, "status", ""), " ")
	title := strings.Trim(helpers.Req(r, "title", ""), " ")

	if title == "" {
		api.Respond(w, r, api.Error("title is required field"))
		return
	}

	article := &db.Article{}

	db.ArticleCreate(article)

	if article == nil {
		api.Respond(w, r, api.Error("Article failed to be created"))
		return
	}

	api.Respond(w, r, api.SuccessWithData("Article saved successfully", map[string]interface{}{"article_id": article.ID}))
	return
}

func articleList(c *fiber.Ctx) error {
	perPage, _ := strconv.ParseUint(utils.FiberReq(c, "per_page", "20"), 10, 64)
	page, _ := strconv.ParseUint(utils.FiberReq(c, "page", "1"), 10, 64)
	if page > 0 {
		page-- // convert to 0 base
	}
	orderBy := strings.ToLower(utils.FiberReq(c, "by", "created"))
	sort := strings.ToLower(utils.FiberReq(c, "sort", "desc"))
	search := utils.FiberReq(c, "search", "")
	offset := page * perPage

	if orderBy == "created_at" || orderBy == "created" || orderBy == "createdat" {
		orderBy = "created"
	}
	if orderBy == "updated_at" || orderBy == "updated" || orderBy == "updatedat" {
		orderBy = "updated"
	}
	if orderBy == "published_at" || orderBy == "published" {
		orderBy = "published"
	}

	if sort != "asc" && sort != "desc" {
		sort = "asc"
	}

	articles := db.ArticleList(offset, uint64(perPage), search, orderBy, sort)

	dataArticles := []map[string]string{}

	for _, article := range articles {
		translation := db.ArticleTranslationFindByArticleIDAndLanguage(article.ID, "en")
		dataArticle := map[string]string{}
		dataArticle["id"] = article.ID

		if translation == nil {
			dataArticle["title"] = ""
			dataArticle["content"] = ""
		} else {
			dataArticle["status"] = article.Status
			dataArticle["title"] = translation.Title
			dataArticle["created_at"] = article.CreatedAt.String()
			dataArticle["updated_at"] = article.UpdatedAt.String()
			dataArticle["published_at"] = article.PublishedAt.String()
		}

		dataArticles = append(dataArticles, dataArticle)
	}

	totalArticles := db.ArticleCount()

	data := map[string]interface{}{
		"articles": dataArticles,
		"per_page": perPage,
		"page":     (page + 1),
		"pages":    math.Ceil(float64(totalArticles) / float64(perPage)),
		"total":    totalArticles,
	}

	// api.Respond(w, r, api.SuccessWithData("Articles list successful", data))
	// return

	return c.JSONP(fiber.Map{
		"status":  "success",
		"message": "Found articles",
		"data":    data,
	})
}

// articleFind finds and article by ID
func articleFind(w http.ResponseWriter, r *http.Request) {
	articleID := strings.Trim(helpers.Req(r, "article_id", ""), " ")

	if articleID == "" {
		api.Respond(w, r, api.Error("article_id is required field"))
		return
	}

	log.Println(" - Article ID: " + articleID)

	article := db.ArticleFindByID(articleID)

	if article == nil {
		api.Respond(w, r, api.Error("Article not found"))
		return
	}

	log.Println(article)

	translation := db.ArticleTranslationFindByArticleIDAndLanguage(article.ID, "en")

	if translation == nil {
		api.Respond(w, r, api.Error("Translation not found"))
		return
	}

	dataArticle := map[string]string{}
	dataArticle["id"] = article.ID
	dataArticle["status"] = article.Status
	dataArticle["wysiwyg"] = article.Wysiwyg
	dataArticle["title"] = translation.Title
	dataArticle["content"] = translation.Content
	dataArticle["created_at"] = article.CreatedAt.String()
	dataArticle["updated_at"] = article.UpdatedAt.String()
	dataArticle["published_at"] = article.PublishedAt.String()

	data := map[string]interface{}{
		"article": dataArticle,
	}

	api.Respond(w, r, api.SuccessWithData("Article found successfully", data))
	return
}

// articleUpdate updates the article
func articleUpdate(w http.ResponseWriter, r *http.Request) {
	articleID := strings.Trim(helpers.Req(r, "article_id", ""), " ")
	status := strings.Trim(helpers.Req(r, "status", ""), " ")
	title := strings.Trim(helpers.Req(r, "title", ""), " ")
	content := strings.Trim(helpers.Req(r, "content", ""), " ")
	wysiwyg := strings.Trim(helpers.Req(r, "wysiwyg", ""), " ")

	if articleID == "" {
		api.Respond(w, r, api.Error("article_id is required field"))
		return
	}

	articleFound := db.ArticleFindByID(articleID)

	if articleFound == nil {
		api.Respond(w, r, api.Error("Article not found"))
		return
	}

	if status != "" {
		isSuccess := db.ArticleUpdateByID(articleID, map[string]string{"Status": status})
		if isSuccess == false {
			api.Respond(w, r, api.Error("Article status failed to be saved"))
			return
		}
	}

	if wysiwyg != "" {
		isSuccess := db.ArticleUpdateByID(articleID, map[string]string{"Wysiwyg": wysiwyg})
		if isSuccess == false {
			api.Respond(w, r, api.Error("Article wysiwyg failed to be saved"))
			return
		}
	}

	if title != "" {
		isSuccess := db.ArticleTranslationUpdate(articleID, "en", map[string]string{"Title": title})
		if isSuccess == false {
			api.Respond(w, r, api.Error("Article title failed to be saved"))
			return
		}
	}

	if content != "" {
		isSuccess := db.ArticleTranslationUpdate(articleID, "en", map[string]string{"Content": content})
		if isSuccess == false {
			api.Respond(w, r, api.Error("Article content failed to be saved"))
			return
		}
	}

	article := db.ArticleFindByID(articleID)

	translation := db.ArticleTranslationFindByArticleIDAndLanguage(articleID, "en")
	if translation == nil {
		api.Respond(w, r, api.Error("Translation not found"))
		return
	}

	dataArticle := map[string]string{}
	dataArticle["id"] = article.ID
	dataArticle["status"] = article.Status
	dataArticle["wysiwyg"] = article.Wysiwyg
	dataArticle["title"] = translation.Title
	dataArticle["content"] = translation.Content
	dataArticle["created_at"] = article.CreatedAt.String()
	dataArticle["updated_at"] = article.UpdatedAt.String()
	dataArticle["published_at"] = article.PublishedAt.String()

	data := map[string]interface{}{
		"article": dataArticle,
	}

	api.Respond(w, r, api.SuccessWithData("Article updated successfully", data))
	return
}
