package user

import (
	"net/http"
	"strings"

	"sinevia.com/app/helpers"

	"github.com/gouniverse/api"
	"sinevia.com/app/db"
)

// ArticleFind authenticates the login user
func ArticleFind(w http.ResponseWriter, r *http.Request) {
	token := strings.Trim(helpers.Req(r, "token", ""), " ")
	articleID := strings.Trim(helpers.Req(r, "article_id", ""), " ")

	if token == "" {
		api.Respond(w, r, api.Error("Token is required field"))
		return
	}

	if db.CacheGet(token, "") == "" {
		api.Respond(w, r, api.Unauthorized("Authentication failed"))
		return
	}

	if articleID == "" {
		api.Respond(w, r, api.Error("article_id is required field"))
		return
	}

	article := db.ArticleFindByID(articleID)

	if article == nil {
		api.Respond(w, r, api.Error("Article not found"))
		return
	}

	translation := db.ArticleTranslationFindByArticleIDAndLanguage(article["Id"], "en")

	if translation == nil {
		api.Respond(w, r, api.Error("Translation not found"))
		return
	}

	dataArticle := map[string]string{}
	dataArticle["id"] = article["Id"]
	dataArticle["status"] = article["Status"]
	dataArticle["wysiwyg"] = article["Wysiwyg"]
	dataArticle["title"] = translation["Title"]
	dataArticle["content"] = translation["Content"]
	dataArticle["created_at"] = article["Created"]
	dataArticle["updated_at"] = article["Updated"]
	dataArticle["published_at"] = article["Published"]

	data := map[string]interface{}{
		"article": dataArticle,
	}

	api.Respond(w, r, api.SuccessWithData("Article found successfully", data))
	return
}
