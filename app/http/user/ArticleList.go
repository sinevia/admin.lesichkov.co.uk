package user

import (
	"math"
	"net/http"
	"strconv"
	"strings"

	"sinevia.com/app/helpers"

	"github.com/gouniverse/api"
	"sinevia.com/app/db"
)

// ArticleList authenticates the login user, pages are calculated starting from 1
func ArticleList(w http.ResponseWriter, r *http.Request) {
	token := strings.Trim(helpers.Req(r, "token", ""), " ")
	perPage, _ := strconv.ParseUint(strings.Trim(helpers.Req(r, "per_page", "20"), " "), 10, 64)
	page, _ := strconv.ParseUint(strings.Trim(helpers.Req(r, "page", "1"), " "), 10, 64)
	if page > 0 {
		page-- // convert to 0 base
	}
	orderBy := strings.ToLower(strings.Trim(helpers.Req(r, "by", "created"), " "))
	sort := strings.ToLower(strings.Trim(helpers.Req(r, "sort", "desc"), " "))
	search := strings.Trim(helpers.Req(r, "search", ""), " ")
	offset := page * perPage

	if token == "" {
		api.Respond(w, r, api.Error("Token is required field"))
		return
	}

	if db.CacheGet(token, "") == "" {
		api.Respond(w, r, api.Unauthorized("Authentication failed"))
		return
	}

	if orderBy == "created_at" || orderBy == "created" || orderBy == "createdat" {
		orderBy = "created"
	}
	if orderBy == "updated_at" || orderBy == "updated" || orderBy == "updatedat" {
		orderBy = "updated"
	}
	if orderBy == "published_at" || orderBy == "published" {
		orderBy = "published"
	}

	if sort != "asc" && sort != "desc" {
		sort = "asc"
	}

	articles := db.ArticleList(offset, uint64(perPage), search, orderBy, sort)

	dataArticles := []map[string]string{}

	for _, article := range articles {
		translation := db.ArticleTranslationFindByArticleIDAndLanguage(article["Id"], "en")
		dataArticle := map[string]string{}
		dataArticle["id"] = article["Id"]

		if translation == nil {
			dataArticle["title"] = ""
			dataArticle["content"] = ""
		} else {
			dataArticle["status"] = article["Status"]
			dataArticle["title"] = translation["Title"]
			dataArticle["created_at"] = article["Created"]
			dataArticle["updated_at"] = article["Updated"]
			dataArticle["published_at"] = article["Published"]
		}

		dataArticles = append(dataArticles, dataArticle)
	}

	totalArticles := db.ArticleCount()

	data := map[string]interface{}{
		"articles": dataArticles,
		"per_page": perPage,
		"page":     (page + 1),
		"pages":    math.Ceil(float64(totalArticles) / float64(perPage)),
		"total":    totalArticles,
	}

	api.Respond(w, r, api.SuccessWithData("Articles list successful", data))
	return
}
