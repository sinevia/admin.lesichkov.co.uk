package http

import (
	"net/http"

	"github.com/gouniverse/api"
	"sinevia.com/app/helpers"
)

// Home shows the default response
func Home(w http.ResponseWriter, r *http.Request) {
	helpers.Respond(w, api.SuccessWithData("The API is working", map[string]interface{}{
		"environment": helpers.AppEnv(),
	}))
}
