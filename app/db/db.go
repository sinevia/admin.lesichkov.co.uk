package db

import (
	"log"
	"os"
	"time"

	"gorm.io/gorm"
	"gorm.io/gorm/logger"

	// Needed for MySQL
	"gorm.io/driver/mysql"
	"gorm.io/driver/sqlite"
)

var databaseInstance *gorm.DB

// Init Initializes the DB connection
func Initialize() {
	log.Printf("getting DB environment variables...")
	dbDrvr := os.Getenv("DB_DRIVER")
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbName := os.Getenv("DB_DATABASE")
	dbUser := os.Getenv("DB_USERNAME")
	dbPass := os.Getenv("DB_PASSWORD")

	log.Printf("Opening database...")

	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold: time.Second,   // Slow SQL threshold
			LogLevel:      logger.Silent, // Log level
			Colorful:      false,         // Disable color
		},
	)

	var db *gorm.DB
	var err error
	if dbDrvr == "sqlite" {
		db, err = gorm.Open(sqlite.Open(dbName), &gorm.Config{})
	}
	if dbDrvr == "mysql" {
		// refer https://github.com/go-sql-driver/mysql#dsn-data-source-name for details
		dbDNS := dbUser + ":" + dbPass + "@tcp(" + dbHost + ":" + dbPort + ")/" + dbName + "?charset=utf8mb4&parseTime=True&loc=Local"
		log.Printf(dbDNS)
		db, err = gorm.Open(mysql.Open(dbDNS), &gorm.Config{
			Logger: newLogger,
		})
	}
	//defer db.Close()

	if err != nil {
		panic("Failed to connect to the database")
	}

	databaseInstance = db

	//db.AutoMigrate(&Cache{})
	//db.AutoMigrate(&Session{})
	//db.AutoMigrate(&User{})
	//db.AutoMigrate(&Entity{})
	//db.AutoMigrate(&EntityAttribute{})

	go CacheExpireJobGoroutine()
	go SessionExpireJobGoroutine()
}

// GetDb returns a DB instance
func GetDb() *gorm.DB {
	return databaseInstance
}
