package db

import (
	"errors"
	"fmt"
	"time"

	"github.com/gouniverse/uid"
	"gorm.io/gorm"
)

const (
	UserStatusActive          = "active"
	UserStatusInactive        = "inactive"
	UserStatusEmailUnverified = "email_unverified"
	RoleUser                  = "user"
	RoleAdmin                 = "admin"
)

// User type
type User struct {
	ID              string     `json:"id" gorm:"type:varchar(40);column:id;primary_key;"`
	Status          string     `json:"status" gorm:"type:varchar(40);column:status;DEFAULT NULL;"`
	FirstName       string     `json:"first_name" gorm:"type:varchar(40);column:first_name;"`
	MiddleNames     string     `json:"last_names" gorm:"type:varchar(40);column:middle_names;DEFAULT NULL;"`
	LastName        string     `json:"middle_names" gorm:"type:varchar(40);column:last_name;"`
	Email           string     `json:"email" gorm:"type:varchar(100);column:email;unique_index"`
	Role            string     `json:"role" gorm:"size:40;column:role;DEFAULT NULL;"`
	Password        string     `json:"password" gorm:"type:varchar(255);column:password;DEFAULT NULL;"`
	EmailVerifiedAt *time.Time `json:"email_verified_at" gorm:"type:datetime;column:created_at;DEFAULT NULL;"`
	CreatedAt       time.Time  `json:"created_at" gorm:"type:datetime;column:created_at;DEFAULT NULL;"`
	UpdatedAt       time.Time  `json:"updated_at" gorm:"type:datetime;column:updated_at;DEFAULT NULL;"`
	DeletedAt       *time.Time `json:"deleted_at" gorm:"type:datetime;olumn:deleted_at;DEFAULT NULL;"`
}

// TableName teh name of the User table
func (User) TableName() string {
	return "snv_users_user"
}

// BeforeCreate adds UID to model
func (u *User) BeforeCreate(tx *gorm.DB) (err error) {
	uuid := uid.NanoUid()
	fmt.Println(uuid)
	u.ID = uuid
	return nil
}

// UserFindByEmail finds a user by email
func UserFindByEmail(email string) *User {

	user := &User{}
	result := GetDb().Where("email = ?", email).First(&user)

	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return nil
	}

	return user
}

// UserFindByID finds a user by ID
func UserFindByID(id string) *User {

	user := &User{}
	result := GetDb().Where("id = ?", id).First(&user)

	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		return nil
	}

	return user
}
