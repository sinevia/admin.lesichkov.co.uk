package db

import (
	"time"
)

// Node type
type Node struct {
	ID        string     `gorm:"type:varchar(40);column:id;primary_key;"`
	ParentID  string     `gorm:"type:varchar(40);column:parent_id;"`
	Key       string     `gorm:"type:varchar(40);column:type;"`
	Value     string     `gorm:"type:longtext;column:description;"`
	CreatedAt time.Time  `gorm:"type:datetime;column:created_at;DEFAULT NULL;"`
	UpdatedAt time.Time  `gorm:"type:datetime;column:updated_at;DEFAULT NULL;"`
	DeletedAt *time.Time `gorm:"type:datetime;olumn:deleted_at;DEFAULT NULL;"`

	Children []Node `gorm:"foreignkey:ParentID"`
}

// TableName teh name of the User table
func (Node) TableName() string {
	return "snv_nodes_node"
}
