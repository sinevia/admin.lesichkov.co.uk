package helpers

import (
	"bytes"
	"html/template"
	"log"
	"net/smtp"
	"os"

	"github.com/darkoatanasovski/htmltags"
	"github.com/jordan-wright/email"
)

// EmailSend sends an email
func EmailSend(from string, to []string, subject string, htmlMessage string) (bool, error) {
	//drvr := os.Getenv("MAIL_DRIVER")
	host := os.Getenv("MAIL_HOST")
	port := os.Getenv("MAIL_PORT")
	user := os.Getenv("MAIL_USERNAME")
	pass := os.Getenv("MAIL_PASSWORD")
	addr := host + ":" + port

	nodes, errStripped := htmltags.Strip(htmlMessage, []string{}, true)

	textMessage := ""

	if errStripped == nil {
		//nodes.Elements   //HTML nodes structure of type *html.Node
		textMessage = nodes.ToString() //returns stripped HTML string
	}

	e := email.NewEmail()
	e.From = from
	e.To = to
	//e.Bcc = []string{"test_bcc@example.com"}
	//e.Cc = []string{"test_cc@example.com"}
	e.Subject = subject
	e.Text = []byte(textMessage)
	e.HTML = []byte(htmlMessage)
	err := e.Send(addr, smtp.PlainAuth("", user, pass, host))

	// Choose auth method and set it up
	// auth := smtp.PlainAuth("", user, pass, host)

	// mime := "MIME-version: 1.0;"
	// mime += "\r\n"
	// mime += "Content-Type: text/html; charset=\"UTF-8\";"

	// msg := "To: " + to[0]
	// msg += "\r\n"
	// msg += "Subject: " + subject
	// msg += "\r\n"
	// msg += mime
	// msg += "\r\n"
	// msg += "\r\n"
	// msg += message
	// msg += "\r\n"

	// err := smtp.SendMail(addr, auth, from, to, []byte(msg))
	if err != nil {
		log.Fatal(err)
		return false, err
	}
	return true, nil
}

// EmailVerificationTemplate returns the template for the email address verification email
func EmailVerificationTemplate(name string, url string) string {
	msg := `
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head></head>
<body>
	<p>
		Hello!
	<p>
	<p>
		Please click the link bellow to verify your email address.
	</p>
	<p>
		<a href="{{.URL}}">Verify Email Address</a>
	</p>
	<p>
		If you did not create an account no further action is required.
	</p>
	<p>
		Thanks,
		<br />
		The Admin Team
	</p>
	<hr />
	<p>
		If you are having trouble clicking the "Verify Email Address" button,
		copy and paste the URL below into your web browser:
		{{.URL}}
	</p>
</body>
<html>
`
	data := struct {
		Name string
		URL  string
	}{
		Name: name,
		URL:  url,
	}
	t, err := template.New("template").Parse(msg)
	if err != nil {
		log.Panic(err.Error())
	}
	var doc bytes.Buffer
	errE := t.Execute(&doc, data)
	if errE != nil {
		log.Panic(errE.Error())
	}
	s := doc.String()
	return s
}

// EmailPasswordChangeTemplate returns the template for the email address verification email
func EmailPasswordChangeTemplate(name string, url string) string {
	msg := `
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head></head>
<body>
	<p>
		Hello!
	<p>
	<p>
		Someone requested to change your password. Please click the link bellow to change it.
	</p>
	<p>
		<a href="{{.URL}}">Change Password</a>
	</p>
	<p>
		If you did not request to change your password no further action is required.
	</p>
	<p>
		Thanks,
		<br />
		The Admin Team
	</p>
	<hr />
	<p>
		If you are having trouble clicking the "Change Password" button,
		copy and paste the URL below into your web browser:
		{{.URL}}
	</p>
</body>
<html>
`
	data := struct {
		Name string
		URL  string
	}{
		Name: name,
		URL:  url,
	}
	t, err := template.New("template").Parse(msg)
	if err != nil {
		log.Panic(err.Error())
	}
	var doc bytes.Buffer
	errE := t.Execute(&doc, data)
	if errE != nil {
		log.Panic(errE.Error())
	}
	s := doc.String()
	return s
}
